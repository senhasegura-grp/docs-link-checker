*** Variables ***
${env}   en

*** Settings ***
Library     Browser
Library     Collections
Library     CSVLibrary
Library     OperatingSystem
Library     String
Library    .venv/lib/python3.11/site-packages/robot/libraries/DateTime.py

*** Variables ***
@{skip_menu}    DevOps Secret Manager > Referências > Referência para aplicações

*** Keywords ***
Is link to be skipped
    [Arguments]   ${item}   @{list}
    FOR    ${element}    IN    @{list}
        IF   $element in $item
            RETURN   ${True}
        END
    END
    RETURN    ${False}

*** Test Cases ***
# test a
#     Set Browser Timeout    30
#     Open Browser    https://docs.senhasegura.io/docs/installation-firewall-requirements
    
#     ${all_pages}=   Get Page IDs
#     ${active_page}=   Get Page IDs   ACTIVE   ACTIVE   ACTIVE
#     ${current_page}=   Get Page IDs   CURRENT   CURRENT   CURRENT
#     Log   ${all_pages}
#     Log   ${active_page}
#     Log   ${current_page}

    
#     ${link} =   Set Variable  (//div[@id="doc_content_block"]//div[@class="content_container_text_sec_in"]//a[not(@class)][text()])[1]
#     Click With Options    ${link}   left   Control
    
#     FOR   ${counter}    IN RANGE    0    30
#         Sleep   0.2
#         ${current_page}=   Get Page IDs
#         ${x} =   Get length   ${current_page}
#         IF   ${x} == 2
#             BREAK
#         END
#     END

#     ${previous} =    Switch Page      NEW

#     ${all_pages}=   Get Page IDs
#     ${active_page}=   Get Page IDs   ACTIVE   ACTIVE   ACTIVE
#     ${current_page}=   Get Page IDs   CURRENT   CURRENT   CURRENT
#     Log   ${all_pages}
#     Log   ${active_page}
#     Log   ${current_page}

#     # ${title}=   Get Title
#     # Log To Console    ${title}
#     # Log To Console    previous: ${previous}

#     # Close Page
#     Switch Page    ${previous}

#     ${all_pages}=   Get Page IDs
#     ${active_page}=   Get Page IDs   ACTIVE   ACTIVE   ACTIVE
#     ${current_page}=   Get Page IDs   CURRENT   CURRENT   CURRENT
#     Log   ${all_pages}
#     Log   ${active_page}
#     Log   ${current_page}

#     FOR    ${page}    IN    @{all_pages}
#         IF   $page != $current_page[0]
#             Close Page   ${page}
#         END
#     END
    
#     # Sleep    3s
#     # # ${old} =  Switch Page    ${previous}
#     # # Log To Console    old ${old}


#     # ${link} =   Set Variable  (//div[@id="doc_content_block"]//div[@class="content_container_text_sec_in"]//a[not(@class)][text()])[2]
#     # Click With Options    ${link}   left   Control
#     # ${previous} =    Switch Page      NEW
#     # ${current_page}=   Get Page IDs
#     # Log   ${current_page}
#     # Close Page
#     # Sleep    2s

time
    ${secs} = 	Get Current Date   result_format=epoch
    ${secs} =   Convert To Integer    ${secs}
    Log To Console    ${secs}

    ${today}=   Evaluate   time.strftime("%Y-%m-%d", time.localtime())   time
    Log To Console    ${today}
