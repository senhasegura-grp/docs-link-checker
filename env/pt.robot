*** Variables ***
${url}   https://docs.senhasegura.io/v3-33/docs/pt/senhasegura
${Upgrade notes}    Upgrade notes
${Changelog}    Changelog v3.32
${General information}    Informações gerais
${Installation}    Instalação
${Settings}    Configurações
${Reports}    Reports
${PAM Core}    PAM Core
${DevOps Secret Manager}    DevOps Secret Manager
${Domum Remote Access}    Domum Remote Access
${GO Endpoint Manager}    GO Endpoint Manager
${Certificate Manager}    Certificate Manager
${Cloud IAM}    Cloud IAM
${Executions}    Executions
${Discovery}    Discovery
${MySafe}    MySafe
${MySafe extension}    Extensão MySafe
${Task Manager}    Task Manager
${Network Connector}    Network Connector
${Load Balancer}    Load Balancer
${Arbitrator}    Arbitrator
${A2A - APIs}    A2A - APIs
${senhasegura mobile app}    Aplicativo móvel senhasegura
${Orbit Config Manager}    Orbit Config Manager
${Protected Information}    Protected Information
${senhasegura SaaS}    Cloud Products > senhasegura SaaS
${Fast Trainings}    Fast Trainings
${Other versions}    Versões anteriores
${Other documents}    Others
${Cloud Security}    Cloud Products > Cloud Security
${Getting Started}   Getting Started
${News}   News
${Produtos senhasegura}   Produtos
${Other senhasegura products}   Outros produtos senhasegura
${senhasegura APIs}   APIs senhasegura
${Other information}   Outras Informações
${Produtos PAM Core}   senhasegura 360 > PAM Core
${Produtos Multi-tenant}    senhasegura 360 > Multi-tenant
${Produtos DevOps Secret Manager}   senhasegura 360 > DevOps Secret Manager
${Produtos Domum Remote Access}   senhasegura 360 > Domum Remote Access
${Produtos GO Endpoint Manager}   senhasegura 360 > GO Endpoint Manager
${Produtos GO Windows}   senhasegura 360 > GO Endpoint Manager > Go for Windows
${Produtos GO Linux}   senhasegura 360 > GO Endpoint Manager > Linux
${Produtos Certificate Manager}   senhasegura 360 > Certificate Manager
${Produtos Cloud IAM}   senhasegura 360 > Cloud IAM
${Citrix Virtual Apps and Desktop}   senhasegura 360 > Citrix Virtual Apps e Desktop
${Produtos Executions}   senhasegura 360 > Executions
${Produtos Discovery}   senhasegura 360 > Discovery
${Produtos MySafe}   senhasegura 360 > MySafe
${Produtos Task Manager}   senhasegura 360 > Task Manager
${Produtos Load Balancer}   senhasegura 360 > Load Balancer
${Produtos Arbitrator}   senhasegura 360 > Arbitrator
${Produtos senhasegura mobile app}   senhasegura 360 > Aplicativo móvel senhasegura
${Produtos Orbit Config Manager}   senhasegura 360 > Orbit Config Manager
${Produtos A2A}   senhasegura 360 > A2A
${Produtos Network Connector}   senhasegura 360 > Network Connector
${Produtos Protected Information}   senhasegura 360 > Protected Information
${Produtos User Behavior}    senhasegura 360 > User Behavior
${Produtos Reports}    senhasegura 360 > Reports
${Produtos Dashboards}    senhasegura 360 > Dashboards
@{skip_menu}   DevOps Secret Manager > Referência > Reference for credentials   
...    User Behavior > Configurações > Referência > Comandos auditados
...    Domum Remote Access > Configurações > Tela inicial de usuários internos > Grupos de usuários internos
