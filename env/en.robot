*** Variables ***
${url}   https://docs.senhasegura.io/v3-33/docs
${Upgrade notes}    Upgrade notes v3.32
${Changelog}    Changelog v3.32
${General information}    General information
${Installation}    Installation
${Settings}   Settings
${Reports}    Reports
${PAM Core}    PAM Core
${DevOps Secret Manager}    DevOps Secret Manager
${Domum Remote Access}    Domum Remote Access
${GO Endpoint Manager}    GO Endpoint Manager
${Certificate Manager}    Certificate Manager
${Cloud IAM}    Cloud IAM
${Executions}    Executions
${Discovery}    Discovery
${MySafe}    MySafe
${MySafe extension}    MySafe extension
${Task Manager}    Task Manager
${Network Connector}    Network Connector
${Load Balancer}    Load Balancer
${Arbitrator}    Arbitrator
${A2A - APIs}    A2A - APIs
${senhasegura mobile app}    senhasegura mobile app
${Orbit Config Manager}    Orbit Config Manager
${Protected Information}    Protected Information
${senhasegura SaaS}    Cloud Products > senhasegura SaaS
${Fast Trainings}    Fast Trainings
${Other versions}    Other versions
${Other documents}    Other documents
${Cloud Security}    Cloud Products > Cloud Security
${Getting Started}   Getting Started
${News}   News
${Produtos senhasegura}   Produtos senhasegura
${Other senhasegura products}   Other senhasegura products
${senhasegura APIs}   senhasegura APIs
${Other information}   Other information
${Produtos PAM Core}    senhasegura 360 > PAM Core
${Produtos Multi-tenant}    senhasegura 360 > Multi-tenant
${Produtos DevOps Secret Manager}    senhasegura 360 > DevOps Secret Manager
${Produtos Domum Remote Access}    senhasegura 360 > Domum Remote Access
${Produtos GO Endpoint Manager}    senhasegura 360 > GO Endpoint Manager
${Produtos GO Windows}   senhasegura 360 > GO Endpoint Manager > Go for Windows
${Produtos GO Linux}   senhasegura 360 > GO Endpoint Manager > Linux
${Produtos Certificate Manager}    senhasegura 360 > Certificate Manager
${Produtos Cloud IAM}    senhasegura 360 > Cloud IAM
${Citrix Virtual Apps and Desktop}   senhasegura 360 > Citrix Virtual Apps and Desktop
${Produtos Executions}    senhasegura 360 > Executions
${Produtos Discovery}    senhasegura 360 > Discovery
${Produtos MySafe}    senhasegura 360 > MySafe
${Produtos Task Manager}    senhasegura 360 > Task Manager
${Produtos Load Balancer}    senhasegura 360 > Load Balancer
${Produtos Arbitrator}    senhasegura 360 > Arbitrator
${Produtos senhasegura mobile app}    senhasegura 360 > senhasegura mobile app
${Produtos Orbit Config Manager}    senhasegura 360 > Orbit Config Manager
${Produtos A2A}    senhasegura 360 > A2A
${Produtos Network Connector}    senhasegura 360 > Network Connector
${Produtos Protected Information}    senhasegura 360 > Protected Information
${Produtos User Behavior}    senhasegura 360 > User Behavior
${Produtos Reports}    senhasegura 360 > Reports
${Produtos Dashboards}    senhasegura 360 > Dashboards
@{skip_menu}   DevOps Secret Manager > Reference > Reference for credentials   
...    User Behavior > Settings > Reference > Audited commands
...    Domum Remote Access > Settings > Internal users > Internal users groups
