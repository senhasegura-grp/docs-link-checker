*** Settings ***
Library    Browser
Resource   keywords.robot

Test Setup   Start setup
Test Teardown   Log variables
Test Timeout    90m

*** Test Cases ***
Open all pages to check if there are broken links
    [Tags]   general
    [Timeout]    3h    
    Search into all links and check if the links are broken

Open Upgrade notes and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Upgrade notes}

Open Changelog and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Changelog}

Open General information and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${General information}

Open Installation and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Installation}

Open Settings and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Settings}

Open Reports and check if there are broken links
    # [Tags]   test_page
    Search for a broken link into   ${Reports}

Open PAM Core and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${PAM Core}

Open DevOps Secret Manager and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${DevOps Secret Manager}

Open Domum Remote Access and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Domum Remote Access}

Open GO Endpoint Manager and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${GO Endpoint Manager}

Open Certificate Manager and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Certificate Manager}

Open Cloud IAM and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Cloud IAM}

Open Executions and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Executions}

Open Discovery and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Discovery}

Open MySafe and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${MySafe}

Open MySafe extension and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${MySafe extension}

Open Task Manager and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Task Manager}

Open Network Connector and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Network Connector}

Open Load Balancer and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Load Balancer}

Open Arbitrator and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Arbitrator}

Open A2A - APIs and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${A2A - APIs}

Open senhasegura mobile app and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${senhasegura mobile app}

Open Orbit Config Manager and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Orbit Config Manager}

Open Protected Information and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Protected Information}

Open senhasegura SaaS and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${senhasegura SaaS}

Open Fast Trainings and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Fast Trainings}

Open Other versions and check if there are broken links
    [Tags]   test_page   beta_page
    Search for a broken link into   ${Other versions}

Open Other documents and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Other documents}

Open Cloud Security and check if there are broken links
    [Tags]   test_page
    Search for a broken link into   ${Cloud Security}

Open Getting Started and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Getting Started}

Open News and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${News}

Open Produtos senhasegura and check if there are broken links
    # [Tags]   beta_page
    Search for a broken link into   ${Produtos senhasegura}

Open Other senhasegura products and check if there are broken links
    # [Tags]   beta_page
    Search for a broken link into   ${Other senhasegura products}

Open senhasegura APIs and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${senhasegura APIs}
    
Open Other information and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Other information}

Open Produtos PAM Core and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos PAM Core}
    
Open Produtos Multi-tenant and check if there are broken links
    # [Tags]   beta_page
    Search for a broken link into   ${Produtos Multi-tenant}

Open Produtos DevOps Secret Manager and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos DevOps Secret Manager}

Open Produtos Domum Remote Access and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Domum Remote Access}

Open Produtos GO Endpoint Manager and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos GO Endpoint Manager}   except_menu=2

Open Produtos GO Windows and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos GO Windows}

Open Produtos GO Linux and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos GO Linux}

Open Produtos Certificate Manager and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Certificate Manager}

Open Produtos Cloud IAM and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Cloud IAM}

Open Produtos Executions and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Executions}

Open Produtos Discovery and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Discovery}

Open Produtos MySafe and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos MySafe}

Open Produtos Task Manager and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Task Manager}

Open Produtos Load Balancer and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Load Balancer}

Open Produtos Arbitrator and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Arbitrator}

Open Produtos senhasegura mobile app and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos senhasegura mobile app}

Open Produtos Orbit Config Manager and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Orbit Config Manager}

Open Produtos A2A and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos A2A}

Open Produtos Network Connector and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Network Connector}

Open Produtos Protected Information and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Protected Information}

Open Produtos User Behavior and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos User Behavior}

Open Produtos Reports and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Reports}

Open Produtos Dashboards and check if there are broken links
    [Tags]   beta_page
    Search for a broken link into   ${Produtos Dashboards}
