*** Settings ***
Library    Browser
Library    Collections
Library    String
Library    CSVLibrary
Library    OperatingSystem
Library    DateTime
Resource    ${CURDIR}/env/${env}.robot

*** Variables ***
${all_menus}    //div[@id="doc_left_sidebar"]//span[@class="article-title"]
${all_content_links}    //div[@id="doc_content_block"]//div[@class="content_container_text_sec_in"]//a[not(@class)][text()]
${next_page}        //div[@class="next-nav"]
${text_page}    //div[@id="doc_content_block"]//div[@class="content_container_text_sec_in"]/div[@class="content_block_head"]
${content_xpath}    (//div[@id="doc_content_block"])[1]//div[@class="content_container_text_sec_in"]
${current_version}    v3-33
${browser_timeout}    60s
${shortest_timeout}   1s
${short_timeout}      3s
${long_timeout}       6s
@{senhasegura_list}
${current_menu}
${menu}   All
${env}   en
@{skip_urls}   caitsith.osdn.jp   community.senhasegura.io   suporte.senhasegura.com.br   .pdf   youtube.com   oid-info.com
...            JavaScripting.com
@{senhasegura_urls}   senhasegura.com   senhasegura.io
@{skip_senhasegura_urls}   deb.senhasegura.com   security.senhasegura.com   cofresenhasegura.com   downloads.senhasegura.io

*** Keywords ***
Start Setup
    Set Browser Timeout  ${browser_timeout}
    
    # Open Browser    ${url}
    New Page    ${url}

    Start variables

Start variables
    @{skipped_list} =   Create List
    @{timeout_list} =   Create List
    @{old_version_list} =   Create List
    @{senhasegura_list} =   Create List
    @{not_found_list} =   Create List
    @{unauthorized_list} =   Create List
    @{portal_link_list} =   Create List

    Set Test Variable    ${current_menu}
    Set Test Variable    @{skipped_list}
    Set Test Variable    @{timeout_list}
    Set Test Variable    @{old_version_list}
    Set Test Variable    @{senhasegura_list}
    Set Test Variable    @{not_found_list}
    Set Test Variable    @{unauthorized_list}
    Set Test Variable    @{portal_link_list}

Log variables
    ${NotEmpty}    Run Keyword And Return Status    Should Not Be Empty      ${skipped_list}
    IF   $NotEmpty
        @{skipped_list} =   Remove Duplicates   ${skipped_list}
        Log   Skipped: ${menu}: ${skipped_list}
    END

    ${NotEmpty}    Run Keyword And Return Status    Should Not Be Empty      ${timeout_list}
    IF   $NotEmpty
        @{timeout_list} =   Remove Duplicates   ${timeout_list}
        Log   Timed out: ${menu}: ${timeout_list}   WARN
        Save in CSV    TimedOut    ${timeout_list}
    END

    ${NotEmpty}    Run Keyword And Return Status    Should Not Be Empty      ${not_found_list}
    IF   $NotEmpty
        @{not_found_list} =   Remove Duplicates   ${not_found_list}
        Log   Not found: ${menu}: ${not_found_list}   ERROR
        Save in CSV    NotFound    ${not_found_list}
    END

    ${NotEmpty}    Run Keyword And Return Status    Should Not Be Empty      ${old_version_list}
    IF   $NotEmpty
        @{old_version_list} =   Remove Duplicates   ${old_version_list}
        Log   Old version: ${menu}: ${old_version_list}   WARN
        Save in CSV    OldVersion    ${old_version_list}
    END

    ${NotEmpty}    Run Keyword And Return Status    Should Not Be Empty      ${senhasegura_list}
    IF   $NotEmpty
        @{senhasegura_list} =   Remove Duplicates   ${senhasegura_list}
        Log   senhasegura.com: ${menu}: ${senhasegura_list}
        Save or Append in CSV    senhasegura    ${senhasegura_list}
    END

    ${NotEmpty}    Run Keyword And Return Status    Should Not Be Empty      ${unauthorized_list}
    IF   $NotEmpty
        @{unauthorized_list} =   Remove Duplicates   ${unauthorized_list}
        Log   Unauthorized: ${menu}: ${unauthorized_list}   ERROR
        Save in CSV    Unauthorized    ${unauthorized_list}
    END

    ${NotEmpty}    Run Keyword And Return Status    Should Not Be Empty      ${portal_link_list}
    IF   $NotEmpty
        @{portal_link_list} =   Remove Duplicates   ${portal_link_list}
        Log   Document 360: ${menu}: ${portal_link_list}   ERROR
        Save in CSV    Document360    ${portal_link_list}
    END

Save in CSV
    [Arguments]   ${type}   ${list}
    
    ${secs} = 	Get Current Date   result_format=epoch
    ${secs} =   Convert To Integer    ${secs}

    ${today}=   Evaluate   time.strftime("%Y-%m-%d", time.localtime())   time

    ${file_path}=   Set Variable   ${CURDIR}/csv/${today}/${menu}-${env}-${type}-${secs}.csv

    Create File    ${file_path}
    Append To Csv File    ${file_path}    ${list}

Save or Append in CSV
    [Arguments]   ${menu}   ${list}

    ${time}=   Evaluate   time.strftime("%Y-%m-%d", time.localtime())   time

    ${file_path}=   Set Variable   ${CURDIR}/csv/${time}/${menu}-${env}.csv
    
    ${not_exists}=   Run Keyword and Return Status   File Should Not Exist   ${file_path}
    IF   ${not_exists}
        Create File    ${file_path}
    END
    Append To Csv File    ${file_path}    ${list}

Save new menu name
    [Arguments]   ${menu_path}   ${new_menu}   ${counter}
    IF   $counter > 1
        ${menu_path}   ${last} =   Split String From Right   ${menu_path}   ${SPACE}>   1
    END
    ${menu_path}=   Set Variable   ${menu_path} > ${new_menu.strip()}
    
    RETURN   ${menu_path}

Search for a broken link into
    [Arguments]   ${menu_name}   ${except_menu}=0
    Set Test Variable   ${menu}    ${menu_name}
    Log To Console    ${SPACE}
    ${menu_name}  ${has_submenu} =   Open menu if necessary    ${menu}
    IF   $has_submenu == "1"
        ${menu_xpath} =    Set variable   //ul[@class="leftsidebarnav"]//li/ul[@style="display: block;"]/li/a/span[normalize-space()="${menu_name}"]/../..
    ELSE
        ${menu_xpath} =    Set variable   //ul[@class="leftsidebarnav"]/li/a/span[normalize-space()="${menu_name}"]/../..
    END
    Wait For Elements State    ${menu_xpath}   message=Menu '${menu_name}' not found
    ${element}   ${size} =   Click and check    ${menu_xpath}   1   ${current_menu}   ${except_menu}

Click and check
    [Arguments]   ${element}   ${size}   ${current_menu}   ${except_menu}=0

    ${main_page}=   Get Page IDs    ACTIVE    ACTIVE    ACTIVE

    # is first menu group?
    ${result} =   Evaluate    '${element}'.endswith(']/../..)[1]/ul/li')
    IF   ${result}
        ${item_qty} =   Set Variable   ${${size} + 1 - ${except_menu}}
    ELSE
        ${item_qty} =   Set Variable   ${${size} + 1}
    END
    FOR    ${counter}    IN RANGE    1    ${item_qty}
        Log To Console   .   no_newline=True
        TRY
            Wait For Elements State   (${element})[${counter}]
            ${has_submenu} =   Get Attribute     (${element})[${counter}]    class
        EXCEPT
            ${has_submenu} =   Set Variable   null
        END
        IF   $has_submenu == "article-title-container default-category"
            ${t} =   Get Text    (${element})[${counter}]
            ${current_menu} =   Save new menu name   ${current_menu}   ${t}   ${counter}
            ${to_skip} =   Is menu to be skipped   ${current_menu}
            IF   ${to_skip} == 1
                CONTINUE
            END
            Log   Clicking into ${t}
            Click    (${element})[${counter}]
            Search and check if the links are broken   ${current_menu}   ${main_page}
            
            ${old_timeout} =    Set Browser Timeout    ${short_timeout}
            ${submenu_xpath} =   Set Variable   (${element})[${counter}]/ul/li
            ${size}=    Get Element Count    ${submenu_xpath}
            Set Browser Timeout   ${old_timeout}
            Click and check    ${submenu_xpath}   ${size}   ${current_menu}   ${except_menu}
        ELSE
            # Não tem um submenu
            ${t} =   Get Text    (${element})[${counter}]
            ${current_menu} =   Save new menu name   ${current_menu}   ${t}   ${counter}
            ${to_skip} =   Is menu to be skipped   ${current_menu}
            IF   ${to_skip} == 0
                Log   Clicking into ${t}
                Click    (${element})[${counter}]
                Search and check if the links are broken   ${current_menu}   ${main_page}
            END
        END
    END

Search and check if the links are broken
    [Arguments]   ${current_menu}   ${main_page}
    ${passed} =   Run Keyword And Return Status   Wait For Elements State    ${content_xpath}   timeout=${short_timeout}
    ${page_url} =   Get Url
    Log   current page: ${page_url}
    IF    $passed == $False
        ${notFound} =   Run Keyword And Return Status   Wait For Elements State    //div[@class="page-not-found-content"]   timeout=1s
        IF   $notFound == $True
            ${current_menu} =   Get Substring    ${current_menu}    3
            Run Keyword And Continue On Failure    Fail    This ${current_menu} is a broken URL
            @{not_found} =   Create List   ${current_menu}   ${page_url}
            Append To List    ${not_found_list}   ${not_found}
            Return to main page    ${main_page}
            ${xpath_base} =   Set variable  (//li[contains(@style, "display: block")]/a[@class="active"]/ancestor::li/following-sibling::li[1])[1]
            Change display attribute    ${xpath_base}/ul   block
        ELSE
            ${old_timeout} =    Set Browser Timeout    ${short_timeout}
            ${size} =   Get Element Count    ${all_content_links}
            Set Browser Timeout   ${old_timeout}
            FOR    ${counter}    IN RANGE    1    ${size} + 1
                Check if the link is broken   (${all_content_links})[${counter}]
            END
        END
    ELSE
        ${old_timeout} =    Set Browser Timeout    ${short_timeout}
        ${size} =   Get Element Count    ${all_content_links}
        Set Browser Timeout   ${old_timeout}
        FOR    ${counter}    IN RANGE    1    ${size} + 1
            Check if the link is broken   (${all_content_links})[${counter}]
        END
    END

Check if the link is broken
    [Arguments]    ${element}
    ${page_url} =   Get Url
    ${link_text} =  Get Text    ${element}
    ${link_url} =   Get Attribute     ${element}    href
    
    ${skip} =   Is link to be skipped   ${link_url}
    IF   ${skip} == 1
        RETURN
    ELSE IF   ${skip} == -1
        @{list} =   Create List   ${menu}   ${page_url}   ${link_text}   ${link_url}
        Append To List    ${senhasegura_list}   ${list}
        RETURN
    END

    IF   $link_text == ""
        Click   ${element}/../../summary
    END
    ${main_page}=   Get Page IDs    ACTIVE    ACTIVE    ACTIVE
    Click With Options    ${element}   left   Control
    TRY
        Wait for new tab
        ${previous} =    Switch Page      NEW
        Wait For Elements State    (//body/*)[1]   attached   ${long_timeout}
        ${link_url} =   Get Url
    EXCEPT
        Run Keyword And Ignore Error   Reload
        ${passed} =   Run Keyword And Return Status   Wait For Elements State    (//body/*)[1]   attached   ${long_timeout}
        IF    $passed == $False
            ${link_url} =   Set Variable   null 
        ELSE
            ${link_url} =   Get Url
        END
        ${previous} =   Set Variable   ${main_page}[0]
    END
    IF   "docs.senhasegura.io" in $link_url and "docs/" in $link_url
        ${passed} =   Run Keyword And Return Status   Wait For Elements State    ${content_xpath}   timeout=${short_timeout}
        IF    $passed == $False
            ${notFound} =   Run Keyword And Return Status   Wait For Elements State    //div[@class="page-not-found-content"]   timeout=${short_timeout}
            IF   $notFound == $True
                Run Keyword And Continue On Failure    Fail    This ${link_url} is a broken URL
                @{not_found} =   Create List   ${page_url}   ${link_text}   ${link_url}
                Append To List    ${not_found_list}   ${not_found}
            ELSE
                Run Keyword And Ignore Error   Reload
                ${unauthorized} =   Run Keyword And Return Status   Wait For Elements State    //h2[text()="You are not authorized to access."]   timeout=${short_timeout}
                IF   $unauthorized == $True
                    Run Keyword And Continue On Failure    Fail    This ${link_url} is an unauthorized url
                    @{unauthorized} =   Create List   ${page_url}   ${link_text}   ${link_url}
                    Append To List    ${unauthorized_list}   ${unauthorized}
                ELSE
                    @{timeout} =   Create List   ${page_url}   ${link_text}   ${link_url}
                    Append To List    ${timeout_list}   ${timeout}
                END
            END
        ELSE IF    "docs.senhasegura.io/${current_version}/docs" not in $link_url and "docs.senhasegura.io/docs" not in $link_url
            @{old_link} =   Create List   ${page_url}   ${link_text}   ${link_url}
            Append To List    ${old_version_list}   ${old_link}
        END
    ELSE IF   "portal.document360.io" in $link_url
        @{portal_link} =   Create List   ${page_url}   ${link_text}   ${link_url}
        Append To List    ${portal_link_list}   ${portal_link}
    ELSE IF   $link_url == "null"
        @{timeout} =   Create List   ${page_url}   ${link_text}   ${link_url}
        Append To List    ${timeout_list}   ${timeout}
    ELSE
        @{skipped} =   Create List   ${page_url}   ${link_text}   ${link_url}
        Append To List    ${skipped_list}   ${skipped}
    END

    Return to main page   ${main_page}

Search into all links and check if the links are broken
    WHILE    ${True}   limit=NONE
        ${main_page}=   Get Page IDs    ACTIVE    ACTIVE    ACTIVE
        Search and check if the links are broken   ${current_menu}   ${main_page}
        ${passed} =   Run Keyword And Return Status   Wait For Elements State    ${next_page}   timeout=${short_timeout}
        IF    $passed == $False
            BREAK
        END
        Click    ${next_page}
    END

Is link to be skipped
    [Arguments]   ${item}
    FOR    ${element}    IN    @{skip_urls}
        IF   $element in $item
            RETURN   1
        END
    END
    FOR    ${element}    IN    @{senhasegura_urls}
        IF   $element in $item
            FOR    ${skip_element}    IN    @{skip_senhasegura_urls}
               IF   $skip_element in $item
                    RETURN   1
               END
            END
            RETURN   -1
        END
    END
    RETURN    0

Is menu to be skipped
    [Arguments]   ${item}
    FOR    ${menu}    IN    @{skip_menu}
        IF   $menu in $item
            RETURN   1
        END
    END
    
    RETURN   0

Wait for new tab
    FOR   ${counter}    IN RANGE    0    30
        Sleep   0.15
        ${current_page}=   Get Page IDs
        ${x} =   Get length   ${current_page}
        IF   $x >= 2
            BREAK
        END
    END

Close all other tabs
    [Arguments]   ${current_page}
    ${all_pages}=   Get Page IDs
    FOR    ${page}    IN    @{all_pages}
        IF   $page != $current_page
            Run Keyword And Ignore Error   Close Page   ${page}
        END
    END

Open menu if necessary
    [Arguments]   ${menu}
    IF    ">" in $menu
        ${items}=   Split String    ${menu}   ${SPACE}>${SPACE}
        ${x} =   Get length   ${items}
        FOR   ${counter}    IN RANGE    0    ${x} - 1
            IF   $counter == 0
                Click   //ul[@class="leftsidebarnav"]/li/a/span[text()="${items}[${counter}]"]/../..
            ELSE
                Click   //ul[@class="leftsidebarnav"]//li/ul[@style="display: block;"]/li/a/span[text()="${items}[${counter}]"]/../..
            END
        END
        RETURN    ${items}[-1]   1
    END

    RETURN   ${menu}   0

Return to main page
    [Arguments]   ${main_page}
   
    ${current_page}=   Get Page IDs    ACTIVE    ACTIVE    ACTIVE
    ${all_pages}=   Get Page IDs    ALL   ALL   ALL
    ${x} =   Get length   ${all_pages}
    IF   $main_page[0] != $current_page[0] and $x >= 2
        Switch Page    ${main_page}[0]
        Close all other tabs   ${main_page}[0]
    ELSE IF   $x == 1
        Go Back
        Reload
    END

Change display attribute
    [Arguments]   ${locator}   ${new_value}
    Evaluate Javascript   ${locator}   (elem) => elem.style.display='${new_value}'